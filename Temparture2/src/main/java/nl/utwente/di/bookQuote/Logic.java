package nl.utwente.di.bookQuote;

public class Logic {

    public String calculator(String value) {
        return String.valueOf((Integer.parseInt(value) * 1.8000) + 32.00);
    }
}
